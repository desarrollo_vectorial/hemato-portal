<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
    <!-- AGREGA TUS ESTILOS -->
    <meta name="theme-color" content="#00B1CD" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
    <!--<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png"/>-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php bloginfo('template_url') ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <style>
        @import url('https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i');
    </style>
    <!-- AGREGA TUS SCRIPTS -->
    <script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/my-functions.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>
    <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
<header>
    <?php
    // VARS ACF HEADER
    $logo = get_field('logo_header', 'option');
    ?>
    <div class="top-bar">
        <nav class="z-depth-0">
            <i class="right material-icons" id="search-icon">&#xE8B6;</i>
            <?php
            wp_nav_menu( array(
                'container' => div,
                'items_wrap' => '<ul class="right">%3$s</ul>',
                'theme_location' => 'top-menu',
                'container_id' => 'topbar',
                'walker' => new CSS_Menu_Walker()
            ));
            ?>
            <div id="search-menu">
                <div class="wrapper">
                    <form role="search" method="get" id="form" action="/">
                        <input id="popup-search" type="search" name="s" placeholder="Buscar" />
                        <button id="popup-search-button" type="submit" value="search"><i class="right material-icons hide-on-med-and-down">&#xE8B6;</i></button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
    <div class="navbar-fixed" id="header-nav">
        <nav class="no-fix">
            <div class="nav-wrapper" id="principal-nav">
                <a href="/" class="brand-logo">
                    <img src="<?php if( $logo ): ?><?php echo $logo; ?><?php else: ?><?php bloginfo('template_url') ?>/assets/images/logo-hematooncologos.png<?php endif; ?>" alt="LOGO HEMATO ONCÓLOGOS">
                </a>
                <!--<i id="search" class="right material-icons hide-on-med-and-down">&#xE8B6;</i>-->
                <?php
                wp_nav_menu( array(
                    'container' => div,
                    'items_wrap' => '<ul class="right hide-on-med-and-down">%3$s</ul>',
                    'theme_location' => 'menu-principal',
                    'container_id' => 'cssmenu',
                    'walker' => new CSS_Menu_Walker()
                ));
                ?>
                <a href="#" data-activates="slide-out-l" class="button-collapse slide-out-l"><i class="material-icons">&#xE5D2;</i></a>
            </div>
        </nav>
    </div>
    <?php
    wp_nav_menu( array(
        'container' => false,
        'items_wrap' => '<ul id="slide-out-l" class="side-nav">%3$s</ul>',
        'theme_location' => 'menu-principal',
        'walker' => new CSS_Menu_Walker()
    ));
    ?>
    <div class="rrss-header">
        <?php if( have_rows('redes_sociales', 'option') ): ?>
            <ul>
                <?php while( have_rows('redes_sociales', 'option') ): the_row();
                    $RRSS = get_sub_field('redes');
                    ?>
                    <li>
                        <?php if ($RRSS == 'Facebook' ): ?>
                            <a href="https://www.facebook.com/HematoOncologos" target="_blank"><i class="fa fa-facebook fa-lg custom-icon"></i></a>
                        <?php elseif ( $RRSS == 'Twitter' ): ?>
                            <a href="https://twitter.com/hematooncologos" target="_blank"><i class="fa fa-twitter fa-lg custom-icon"></i></a>
                        <?php elseif ( $RRSS == 'Linkedin' ): ?>
                            <a href="https://co.linkedin.com/company/hemato-oncologos-s-a" target="_blank"><i class="fa fa-linkedin fa-lg custom-icon"></i></a>
                        <?php elseif ( $RRSS == 'Youtube' ): ?>
                            <a href="https://www.youtube.com/user/hematooncologos" target="_blank"><i class="fa fa-youtube-play fa-lg custom-icon"></i></a>
                        <?php elseif ( $RRSS == 'Instagram' ): ?>
                            <a href="#No-Tienen-Instagram" target="_blank"><i class="fa fa-instagram fa-lg custom-icon"></a>
                        <?php else: ?>
                            <?php    echo 'SELECCIONA LAS REDES';    ?>
                        <?php endif ?>
                    </li>
                <?php endwhile; wp_reset_postdata(); ?>
            </ul>
        <?php endif; ?>
    </div>
</header>
