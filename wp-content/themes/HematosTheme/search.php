<?php
get_header();
wp_head();
?>
<?php $post_id = 106; ?>
<div class="container-fluid">
    <div class="row" id="banner" style="background-image: url( <?php echo get_the_post_thumbnail_url($post_id); ?> );">
        <div class="col title">
            <h1> RESULTADOS DE LA BUSQUEDA</h1>
        </div>
        <div class="col arrow hide-on-med-and-down">
            <a href="#content-internas"><i class="hemato-dauntupabajo"></i></a>
        </div>
    </div>
    <section id="breadcrumbs" class="hide-on-med-and-down">
        <nav class="z-depth-0">
            <div class="nav-wrapper">
                <div class="col l7 s12">
                    <a class="breadcrumb" href="http://hemato.portal.vec" title=""><i class="hemato-home"></i></a>
                    <a class="breadcrumb"><span> RESULTADOS DE LA BUSQUEDA</span></a>
                </div>
            </div>
        </nav>
    </section>
</div>
<div class="container-fluid" id="content-internas">
    <div class="row">
        <div class="col l8 s12 allcontent">
            <section id="buscador">
                <div class="container-fluid">
                    <div class="row">
                        <?php
                        if (have_posts()) {
                            while (have_posts()) {
                                the_post();
                                ?>
                                <div class="row buscador">
                                    <div class="col m9 content-text">
                                        <div class="">
                                            <h3 class="title"><?php echo the_title(); ?></h3>
                                            <p><?php echo the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                    <div class="col m3 content-btn">
                                        <a href="<?php echo the_permalink(); ?>" class="waves-effect waves-light btn z-depth-0">LEER MÁS</a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        else {
                            ?>
                            <h3 align="center">Lo sentimos, no se encontraron resultados para esta búsqueda</h3>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col l4 s12 sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>



<?php
wp_footer();
get_footer();
?>
