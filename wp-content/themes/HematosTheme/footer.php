<?php
// VARS ACF FOOTER
$logo_footer = get_field('logo_footer', 'option');
$logo_vigilado = get_field('logo_footer', 'option');
$proteccion_de_datos = get_field('proteccion_de_datos', 'option');
$url_proteccion_de_datos = get_field('url_proteccion_de_datos', 'option');
$copy_footer = get_field('copyright', 'option');
?>
    <footer>
    <div class="row">
    <div class="col s12">
    <div class="col l2 s12 vertical-menu">
        <h2>INICIO</h2>
        <?php
        wp_nav_menu( array(
            'container' => false,
            'items_wrap' => '<ul>%3$s</ul>',
            'theme_location' => 'footer-inicio'
        ));
        ?>
    </div>
    <div class="col l2 s12 vertical-menu">
        <h2>SERVICIOS</h2>
        <?php
        wp_nav_menu( array(
            'container' => false,
            'items_wrap' => '<ul>%3$s</ul>',
            'theme_location' => 'footer-servicio'
        ));
        ?>
    </div>
    <div class="col l4 s12 sedes">
        <h2>SEDES</h2>
        <?php if( have_rows('sedes', 'option') ): ?>
            <div class="owl-carousel owl-theme">
                <?php while( have_rows('sedes', 'option') ): the_row();
                    $foto = get_sub_field('foto');
                    $ciudad_y_pais = get_sub_field('ciudad_y_pais');
                    $direccion = get_sub_field('direccion');
                    $telefono = get_sub_field('telefono');
                    ?>
                    <div class="item">
                        <div class="col l5 s12">
                            <img src="<?php if( $foto ): ?><?php echo $foto; ?><?php else: ?>http://via.placeholder.com/130x130<?php endif; ?>" alt="">
                        </div>
                        <div class="col l7 s12">
                            <h3><?php echo $ciudad_y_pais; ?></h3>
                            <p><?php echo $direccion; ?></p>
                            <p><?php echo $telefono; ?></p>
                        </div>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col l3 s12 iconos">
    <div class="col s12 logo-hemato">
        <img src="<?php if( $logo_footer ): ?><?php echo $logo_footer; ?><?php else: ?><?php bloginfo('template_url') ?>/assets/images/logo-hematooncologos-blanco.png<?php endif; ?>" alt="">
    </div>
    <div class="col s9 l6 rrss-footer">
<?php if( have_rows('redes_sociales', 'option') ): ?>
    <ul>
    <?php while( have_rows('redes_sociales', 'option') ): the_row();
        $RRSS = get_sub_field('redes');
        ?>
        <li>
        <?php if ($RRSS == 'Facebook' ): ?>
            <a href="https://www.facebook.com/HematoOncologos" target="_blank"><i class="fa fa-facebook fa-lg custom-icon"></i></a>
        <?php elseif ( $RRSS == 'Twitter' ): ?>
            <a href="https://twitter.com/hematooncologos" target="_blank"><i class="fa fa-twitter fa-lg custom-icon"></i></a>
        <?php elseif ( $RRSS == 'Linkedin' ): ?>
            <a href="https://co.linkedin.com/company/hemato-oncologos-s-a" target="_blank"><i class="fa fa-linkedin fa-lg custom-icon"></i></a>
        <?php elseif ( $RRSS == 'Youtube' ): ?>
            <a href="https://www.youtube.com/user/hematooncologos" target="_blank"><i class="fa fa-youtube-play fa-lg custom-icon"></i></a>
        <?php elseif ( $RRSS == 'Instagram' ): ?>
            <a href="#No-Tienen-Instagram" target="_blank"><i class="fa fa-instagram fa-lg custom-icon"></a>
        <?php else: ?>
            <?php    echo 'SELECCIONA LAS REDES';    ?>
        <?php endif ?>
            </li>
            <?php endwhile; wp_reset_postdata(); ?>
            </ul>
        <?php endif; ?>
        </div>
        <div class="col s3 l6 logo-super">
            <img src="<?php if( $logo_vigilado ): ?><?php echo $logo_vigilado; ?><?php else: ?><?php bloginfo('template_url') ?>/assets/images/logo-supersalud.png<?php endif; ?>" alt="">
        </div>
        <div class="col s12 terminos">
            <a href="<?php echo $url_proteccion_de_datos; ?>" target="_blank"><i class="hemato-sarlaft"></i> <?php echo $proteccion_de_datos; ?></a>
        </div>
        </div>
        </div>
        </div>
        <div class="row copy">
            <div class="col s12">
                <p><?php echo $copy_footer; ?></p>
            </div>
        </div>
        </footer>

        <?php wp_footer(); ?>
        </body>
        </html>