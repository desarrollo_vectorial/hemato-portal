jQuery(function ($) {
    //slide nav left
    $('.slide-out-l').sideNav({
            menuWidth: 300,
            edge: 'left',
            closeOnClick: false
        }
    );
    $('.slide-out-r').sideNav({
            menuWidth: 300,
            edge: 'right',
            closeOnClick: false
        }
    );
    $('.collapsible').collapsible()

    $('#search-menu').removeClass('toggled');

    $('#search-icon').click(function(e) {
        e.stopPropagation();
        $('#search-menu').toggleClass('toggled');
        $("#popup-search").focus();
    });

    $('#search-menu input').click(function(e) {
        e.stopPropagation();
    });

    $('#search-menu, body').click(function() {
        $('#search-menu').removeClass('toggled');
    });

    $(document).ready(function() {
        $('select').material_select();
    });

    $('.has-sub a').on('click', function(){
        $(this).next().toggleClass('active');
        $(this).parent().siblings().find('ul').removeClass('active');
    });

    $('.owl-carousel').owlCarousel({
        center: true,
        items:1,
        dots:true,
        nav:false,
        loop:true,
    });

    $(function() {
        var header = $(".no-fix");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 40) {
                header.removeClass('no-fix').addClass('fix');
            } else {
                header.removeClass('fix').addClass('no-fix');
            }
        });
    });

    $('a[href^="#content-internas"]').click(function(e) {
        $("html,body").animate(
            { scrollTop: $(this.hash).offset().top - $('#header-nav').height() },
            1000
        );
        return false;
        e.preventDefault();
    });

});

