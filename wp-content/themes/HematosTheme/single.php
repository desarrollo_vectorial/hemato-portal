<?php

/* Template Name: CON SIDEBAR */

?>

<?php

get_header();
wp_head();

?>
<?php $post_id = 106; ?>
<div class="container-fluid">
    <div class="row" id="banner" style="background-image: url( <?php echo get_the_post_thumbnail_url($post_id); ?> );">
        <div class="col title">
            <h1> <?php echo get_the_title($post_id); ?></h1>
        </div>
        <div class="col arrow hide-on-med-and-down">
            <a href="#content-internas"><i class="hemato-dauntupabajo"></i></a>
        </div>
    </div>
    <section id="breadcrumbs" class="hide-on-med-and-down">
        <nav class="z-depth-0">
            <div class="nav-wrapper">
                <div class="col l7 s12">
                    <a class="breadcrumb" href="http://hemato.portal.vec" title=""><i class="hemato-home"></i></a>
                    <a class="breadcrumb"><span> <?php the_title(); ?></span></a>
                </div>
            </div>
        </nav>
    </section>
</div>
<div class="container-fluid" id="content-internas">
    <div class="row">
        <div class="col l8 s12 allcontent">
            <?php if(have_posts()): ?>
                <?php while (have_posts()) : the_post(); ?>
                    <?php $post_date = get_the_date( 'Y-m-j' );?>
                    <h3><?php echo the_title(); ?></h3>
                    <?php
                    date_default_timezone_set('America/Bogota');
                    $date = date('Y-m-j', time());
                    if ($date == $post_date){
                        $hace = new haceTanto($post_date,'h');
                        echo '<p class="single-post-date">';
                        echo 'Hace'.$hace;
                        echo '</p>';
                    }
                    else{
                        $hace = new haceTanto($post_date,'d');
                        echo '<p class="single-post-date">';
                        echo 'Hace'.$hace;
                        echo '</p>';
                    }
                    ?>
                    <?php the_content(); ?>
                <?php endwhile; wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
        <div class="col l4 s12 sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>


<?php

wp_footer();
get_footer();

?>

