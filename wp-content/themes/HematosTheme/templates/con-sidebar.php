<?php

/* Template Name: CON SIDEBAR */

?>

<?php

get_header();
wp_head();

?>
<div class="container-fluid">
    <div class="row" id="banner" style="background-image: url( <?php the_post_thumbnail_url(); ?> );">
        <div class="col title">
            <h1><?php echo get_the_title() ?></h1>
        </div>
        <div class="col arrow hide-on-med-and-down">
            <a href="#content-internas"><i class="hemato-dauntupabajo"></i></a>
        </div>
        <div class="col form-internas hide-on-med-and-down">
            <div class="tabs-name">
                <ul class="tabs tabs-fixed-width">
                    <li class="tab"><a class="active" href="#solicitar">Solicitar cita</a></li>
                    <li class="tab"><a href="#cancelar">Cancelar cita</a></li>
                </ul>
            </div>
            <div class="tabs-content">
                <div id="solicitar" class="form-all col s12">
                    <p>Para solicitar su cita diligencie el siguiente formulario. Los campos marcados con (*) son obligatorios.</p>
                    <form>
                        <div>
                            <div class="input-field col s12">
                                <input id="icon_prefix" type="text" class="validate" placeholder="First Name">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="tel" class="validate" placeholder="Telephone">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="email" class="validate" placeholder="Email">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="text" class="validate" placeholder="Consult">
                            </div>
                            <div class="input-field col s12">
                                <select>
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                </select>
                            </div>
                            <p class="check">
                                <input type="checkbox" id="test5" />
                                <label for="test5">Acepto términos y condiciones</label>
                            </p>
                            <hr>
                            <div class="input-field col s12">
                                <input type="submit" value="Enviar">
                            </div>
                        </div>
                    </form>
                </div>
                <div id="cancelar" class="form-all col s12">
                    <p>Para solicitar su cita diligencie el siguiente formulario. Los campos marcados con (*) son obligatorios.</p>
                    <form>
                        <div>
                            <div class="input-field col s12">
                                <input id="icon_prefix" type="text" class="validate" placeholder="First Name">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="tel" class="validate" placeholder="Telephone">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="email" class="validate" placeholder="Email">
                            </div>
                            <div class="input-field col s12">
                                <input id="icon_telephone" type="text" class="validate" placeholder="Consult">
                            </div>
                            <div class="input-field col s12">
                                <select>
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                </select>
                            </div>
                            <p class="check">
                                <input type="checkbox" id="test5" />
                                <label for="test5">Acepto términos y condiciones</label>
                            </p>
                            <hr>
                            <div class="input-field col s12">
                                <input type="submit" value="Enviar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <section id="breadcrumbs" class="hide-on-med-and-down">
        <nav class="z-depth-0">
            <div class="nav-wrapper">
                <div class="col l7 s12">
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </nav>
    </section>
</div>
<div class="container-fluid" id="content-internas">
    <div class="row">
        <div class="col l8 s12 allcontent">
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    echo the_content();
                }
            }
            ?>
            <?php if( have_rows('tipo_de_contenido') ): ?>
                <div class="row">
                    <div>
                        <?php while( have_rows('tipo_de_contenido') ): the_row();
                            $select = get_sub_field('seleccione_el_contenido_a_mostrar');
                            $texto = get_sub_field('texto_html');
                            $tabs = get_sub_field('tabs');
                            $acordeon = get_sub_field('acordeon');
                            ?>
                            <?php if( $select == 'html' ): ?>
                                <div class="content-internas">
                                    <?php echo $texto; ?>
                                </div>
                            <?php elseif ( $select == 'tabs' ): ?>
                                <div class="content-internas">
                                <?php if( have_rows('tabs') ): ?>
                                    <?php $counter = 1;  //this sets up the counter starting at 0 ?>
                                    <ul id="tabs-swipe" class="tabs tabs-fixed-width z-depth-0">
                                        <?php while( have_rows('tabs') ): the_row();
                                            $active = get_sub_field('principal');
                                            $titulo = get_sub_field('titulo');
                                            $contenido = get_sub_field('contenido');
                                            ?>
                                            <li class="tab"><a class="<?php if( $active == 'si' ) { echo 'active' ;  } ?>" href="#<?php echo $counter;?>"><?php echo $titulo; ?></a></li>
                                            <?php $counter++; // add one per row ?>
                                        <?php endwhile; wp_reset_postdata(); //END WHILE TABS?>
                                    </ul>
                                    </div>
                                    <?php $counter2 = 1;  //this sets up the counter starting at 0 ?>
                                    <?php while( have_rows('tabs') ): the_row();
                                        $contenido = get_sub_field('contenido');
                                        ?>
                                        <div id="<?php echo $counter2;?>" class="col s12">
                                            <?php echo $contenido; ?>
                                        </div>
                                        <?php $counter2++; // add one per row ?>
                                    <?php endwhile; wp_reset_postdata(); //END WHILE TABS CONTENT?>
                                <?php endif; ?>
                            <?php elseif ( $select == 'acordeon' ): ?>
                                <div class="content-internas">
                                    <?php if( have_rows('acordeon') ): ?>
                                        <ul class="collapsible collapsible-accordion z-depth-0" data-collapsible="accordion">
                                            <?php while( have_rows('acordeon') ): the_row();
                                                $active = get_sub_field('principal');
                                                $titulo = get_sub_field('titulo');
                                                $contenido = get_sub_field('contenido');
                                                $icono = get_sub_field('icono');
                                                ?>
                                                <li class="<?php if( $active == 'si' ) { echo 'active' ;  } ?>">
                                                    <div class="collapsible-header <?php if( $active == 'si' ) { echo 'active' ;  } ?>">
                                                        <?php if ($icono == 'corazon' ): ?>
                                                            <span class="hemato-sobrecancer-05"><span class="path1"></span><span class="path2"></span></span>
                                                        <?php elseif ( $icono == 'warning'  ): ?>
                                                            <span class="hemato-sobrecancer-04"><span class="path1"></span><span class="path2"></span></span>
                                                        <?php elseif ( $icono == 'estetoscopio'  ): ?>
                                                            <span class="hemato-sobrecancer-03"><span class="path1"></span><span class="path2"></span></span>
                                                        <?php elseif ( $icono == 'pildoras'  ): ?>
                                                            <span class="hemato-sobrecancer-02"><span class="path1"></span><span class="path2"></span></span>
                                                        <?php elseif ( $icono == 'manos'  ): ?>
                                                            <span class="hemato-sobrecancer-01"><span class="path1"></span><span class="path2"></span></span>
                                                        <?php else: ?>
                                                            <span></span>
                                                        <?php endif ?>
                                                        <i class="material-icons">&#xE313;</i> <p><?php echo $titulo; ?></p>
                                                    </div>
                                                    <div class="collapsible-body">
                                                        <span> <?php echo $contenido; ?></span>
                                                    </div>
                                                </li>
                                            <?php endwhile; wp_reset_postdata(); //END WHILE ACORDEON?>
                                        </ul>
                                    <?php endif;//END IF ACORDEON ?>
                                </div>
                            <?php endif; //END SELECT?>
                        <?php endwhile; wp_reset_postdata(); //END WHILE CONTENIDO?>
                    </div>
                </div>
            <?php endif; //END ROWS CONTENIDO?>
        </div>
        <div class="col l4 s12 sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>


<?php

wp_footer();
get_footer();

?>

