<?php

/* Template Name: BLOG */

?>
<?php

get_header();
wp_head();

?>
<div class="container-fluid">
    <div class="row" id="banner" style="background-image: url( <?php the_post_thumbnail_url(); ?> );">
        <div class="col title">
            <h1><?php echo get_the_title() ?></h1>
        </div>
        <div class="col arrow hide-on-med-and-down">
            <a href="#content-internas"><i class="hemato-dauntupabajo"></i></a>
        </div>
    </div>
    <section id="breadcrumbs" class="hide-on-med-and-down">
        <nav class="z-depth-0">
            <div class="nav-wrapper">
                <div class="col l7 s12">
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </nav>
    </section>
</div>
<div class="container-fluid" id="content-internas">
    <div class="row">
        <div class="col l12 s12 allcontent">
            <div id="blog">
                <?php
                $args = array(
                    'post_type' => 'post' ,
                    'orderby' => 'date' ,
                    'order' => 'DESC' ,
                    'posts_per_page' => 9,
                );
                $news = new WP_Query( $args );
                ?>
                <?php if( $news->have_posts() ) : ?>
                    <?php while( $news->have_posts() ) : $news->the_post(); ?>
                        <div class="col l4 s12">
                            <div class="news-container z-depth-1">
                                <div class="news-image">
                                    <div class="social-network-blog">
                                        <ul>
                                            <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="<?php _e('Compartelo en Facebook!', 'cryst4l')?>"><i class="fa fa-facebook fa-lg"></i></a></li>
                                            <li><a target="_blank" href="http://twitter.com/home?status=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>: <?php the_permalink(); ?>" title="<?php _e('Compartelo en Twitter!', 'cryst4l')?>"><i class="fa fa-twitter fa-lg"></i> </a></li>
                                            <li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" title="<?php _e('Compartelo en Google Plus!', 'cryst4l')?>"><i class="fa fa-google-plus fa-lg"></i></a></li>
                                            <li><a target="_blank" href="https://www.linkedin.com/cws/share?url=<?php the_permalink(); ?>" title="<?php _e('Compartelo en Linkedin!', 'cryst4l')?>"><i class="fa fa-linkedin fa-lg"></i></a></li>
                                        </ul>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if( has_post_thumbnail() ): ?>
                                            <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                        <?php else: ?>
                                            <img src="http://via.placeholder.com/575x575" alt="">
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="news-title">
                                    <?php $post_date = get_the_date( 'Y-m-j' );?>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    <?php
                                    date_default_timezone_set('America/Bogota');
                                    $date = date('Y-m-j', time());
                                    if ($date == $post_date){
                                        $hace = new haceTanto($post_date,'h');
                                        echo '<p class="date">';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    else{
                                        $hace = new haceTanto($post_date,'d');
                                        echo '<p class="date">';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    ?>
                                </div>
                                <div class="news-content">
                                    <p><?php echo the_excerpt(); ?></p>
                                </div>
                                <div class="news-btn">
                                    <a href="<?php the_permalink(); ?>">Ver más ></a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<?php

wp_footer();
get_footer();

?>