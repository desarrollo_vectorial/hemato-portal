<?php
// VARS ACF SIDEBAR
$blog = get_field('blog', 'option');
?>
<div class="sidebar-news">
    <div class="row">
        <div class="col l8 s12 noticia-title">
            <h3>Noticias</h3>
        </div>
        <div class="col l4 s12 noticia-more">
            <a href="<?php echo $blog; ?>">Ver todas > </a>
        </div>
    </div>
    <div class="slider-news">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <?php query_posts('showposts=3'); ?>
                <?php $posts = get_posts('numberposts=3&offset=0'); foreach ($posts as $post) : start_wp(); ?>
                    <?php static $count1 = 0; if ($count1 == "3") { break; } else { ?>
                        <?php $post_date = get_the_date( 'Y-m-j' );?>
                        <div class="row">
                            <div class="col l3 s12 post-image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if( has_post_thumbnail() ): ?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                    <?php else: ?>
                                        <img src="http://via.placeholder.com/575x575" alt="">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="col l9 s12 post-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <?php the_excerpt(); ?>
                                <div class="col s7 post-date">
                                    <?php
                                    date_default_timezone_set('America/Bogota');
                                    $date = date('Y-m-j', time());
                                    if ($date == $post_date){
                                        $hace = new haceTanto($post_date,'h');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    else{
                                        $hace = new haceTanto($post_date,'d');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    ?>
                                </div>
                                <div class="col s5 post-more">
                                    <a href="<?php the_permalink(); ?>">Ver más > </a>
                                </div>
                            </div>
                        </div>
                        <?php $count1++; } ?>
                <?php endforeach; ?>
            </div>
            <div class="item">
                <?php query_posts('showposts=3'); ?>
                <?php $posts = get_posts('numberposts=3&offset=3'); foreach ($posts as $post) : start_wp(); ?>
                    <?php static $count2 = 0; if ($count2 == "3") { break; } else { ?>
                        <?php $post_date = get_the_date( 'Y-m-j' );?>
                        <div class="row">
                            <div class="col l3 s12 post-image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if( has_post_thumbnail() ): ?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                    <?php else: ?>
                                        <img src="http://via.placeholder.com/575x575" alt="">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="col l9 s12 post-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <?php the_excerpt(); ?>
                                <div class="col s7 post-date">
                                    <?php
                                    date_default_timezone_set('America/Bogota');
                                    $date = date('Y-m-j', time());
                                    if ($date == $post_date){
                                        $hace = new haceTanto($post_date,'h');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    else{
                                        $hace = new haceTanto($post_date,'d');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    ?>
                                </div>
                                <div class="col s5 post-more">
                                    <a href="<?php the_permalink(); ?>">Ver más > </a>
                                </div>
                            </div>
                        </div>
                        <?php $count2++; } ?>
                <?php endforeach; ?>
            </div>
            <div class="item">
                <?php query_posts('showposts=3'); ?>
                <?php $posts = get_posts('numberposts=3&offset=6'); foreach ($posts as $post) : start_wp(); ?>
                    <?php static $count3 = 0; if ($count3 == "3") { break; } else { ?>
                        <?php $post_date = get_the_date( 'Y-m-j' );?>
                        <div class="row">
                            <div class="col l3 s12 post-image">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if( has_post_thumbnail() ): ?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                    <?php else: ?>
                                        <img src="http://via.placeholder.com/575x575" alt="">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="col l9 s12 post-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <?php the_excerpt(); ?>
                                <div class="col s7 post-date">
                                    <?php
                                    date_default_timezone_set('America/Bogota');
                                    $date = date('Y-m-j', time());
                                    if ($date == $post_date){
                                        $hace = new haceTanto($post_date,'h');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    else{
                                        $hace = new haceTanto($post_date,'d');
                                        echo '<p>';
                                        echo 'Hace'.$hace;
                                        echo '</p>';
                                    }
                                    ?>
                                </div>
                                <div class="col s5 post-more">
                                    <a href="<?php the_permalink(); ?>">Ver más > </a>
                                </div>
                            </div>
                        </div>
                        <?php $count3++; } ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="sidebar-servicios">
    <h3>NUESTROS SERVICIOS</h3>
    <?php
    wp_nav_menu( array(
        'container' => false,
        'items_wrap' => '<ul>%3$s</ul>',
        'theme_location' => 'servicios'
    ));
    ?>
</div>