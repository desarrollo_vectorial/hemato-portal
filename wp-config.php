<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('LOCAL_DOMAIN', 'hemato.portal.vec');
define('DEV_DOMAIN', 'hemato-portal.vectorial.co');
define('PROD_DOMAIN', 'hemato.vectorialgroup.com');
define('PROD_DOMAIN2', '');
define('DEV_LOCAL_VIEW', 'hemato.192.168.0.40.xip.io');


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

switch (HTTP_HOST) {
    case LOCAL_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://hemato.portal.vec/');
        define('WP_SITEURL','http://hemato.portal.vec/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_hemato_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case DEV_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'hemato_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'vectorial$_$');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case DEV_LOCAL_VIEW:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://hemato.192.168.0.40.xip.io/');
        define('WP_SITEURL','http://hemato.192.168.0.40.xip.io/');

        define('DB_NAME', 'dev_hemato_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case PROD_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://motovalle.vectorialgroup.com/');
        define('WP_SITEURL','http://motovalle.vectorialgroup.com/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'Motovalle_prod');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'motovalle');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'm070v4l$9.wQy3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');
        break;

    case PROD_DOMAIN2:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'webcrc_w3bs1t3');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'webcrc_w3bs1t3');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'crcv4ll3w3bs1t3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;
    default:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', '');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', '');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '');
        break;
}

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'mb;t*z!n=Z=THBehVi{jpqOOa]}6|Q9,7F82`js$J|brp].}UkJEY`=<A.;/hYl-');
define('SECURE_AUTH_KEY', 'Z7UGNI~TJ(ziizx=u81k=$DOR%V%F077-v#_}yJo.F^:+i~ffK_1;=1#sj5%8jdj');
define('LOGGED_IN_KEY', 'LQx5x*}53#0)U>=uT,UWsg*<A--q g<F0f)em:^sa?rdUEa0X6ZnCB80h+~?SJHk');
define('NONCE_KEY', 'N7gPP c$d?*)Oe =KpD_rfI&W;42GFXtW:t~!;zm|{ivl^7.QGJFl z1%T[&WJ~*');
define('AUTH_SALT', 'If3i?Fo%w9ysjFXxjwyjA(HWaJPHDV`4Rr[c!!R)zRtqlVish<y7QWP*ne_p`L)T');
define('SECURE_AUTH_SALT', 'I=WjP`ZBvR?6{#YHpeAlC?^0o]j{kch78k(z4|,3(gsYKbwTV3SF.o4Q[bv#%f1#');
define('LOGGED_IN_SALT', 'y^H/+d*vhBnjt^|oJ.xYPpzM);xL2n.J4v#&7q>aUn_Dv*tvLS:-OPc9YW(c6BVu');
define('NONCE_SALT', ']SbZ%RB6XS#H|)T4H),~T7EP*o#SuFLRZZd{d9v0YAx01emDX_vWKK.}jL&}@9Cz');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'he_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

